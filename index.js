#!/usr/bin/env node

/* global process */
/* eslint "no-console": "off" */

// Libs for running the interactive shell
var chalk = require( "chalk" );
var clear = require( "clear" );
var figlet = require( "figlet" );
var inquirer = require( "inquirer" );

// Libs for running the bootstrapper
var globby = require( "globby" );
var mkdirp = require( "mkdirp" );

// Libs from core
var fs = require( "fs" );
var path = require( "path" );

// path this was started from
var dir = process.cwd();
// name of the current directory
var name = dir.split( path.sep ).pop();

var questions = [
    {
        "name": "name",
        "type": "input",
        "message": "Enter the name of your project: ",
        "default": name
    },
    {
        "name": "root",
        "type": "input",
        "message": "Enter the location to unpack the project to: ",
        "default": dir
    }
];

function configureRuntime( config ){
    return Object.assign( {}, config, {
        "source": __dirname
    } );
}

function calculateSlugs( responses ){
    var projectName = responses.name;
    var projectFilename = projectName
        .toLowerCase()
        .replace( /\s/g, "-" )
        .replace( /[^\w-]/g, "" );
    var projectKey = projectName.replace( /[^\w]/g, "" );

    return Object.assign( {}, responses, {
        "filename": projectFilename,
        "key": projectKey
    } );
}

function preInform( config ){
    console.log(
        chalk.blue( `Using ${config.name} as the project name.` )
    );
    console.log(
        chalk.blue( `Using ${config.filename} as the project name on disk.` )
    );
    console.log(
        chalk.blue( `Using ${config.key} as the project name in code.` )
    );
}

function postInform( config ){
    console.log(
        chalk.white( "\nDone unpacking." )
    );
    console.log(
        chalk.white( "Don't forget the post-unpack steps:\n" )
    );

    if( config.root != process.cwd() ){
        console.log( "•" + chalk.magenta( "Change to the install directory: " ) + chalk.cyan( `cd ${config.root}` ) );
    }

    console.log( "•" + chalk.magenta( "Run your application install: " ) + chalk.cyan( `npm install` ) );
    console.log( "•" + chalk.magenta( "Run an application build: " ) + chalk.cyan( `npm run build` ) );

    console.log( "" );
}

function install( config, asyncDoneBack ){
    var fileList = globby( [
        `${config.source}/strap/**/*`,
        `${config.source}/strap/.*`,
        `!${config.source}/strap/src/content/html/templates.json`,
        `!${config.source}/strap/src/content/routes/routes.json`,
        `!${config.source}/strap/node_modules`,
        `!${config.source}/strap/public`,
        `!${config.source}/strap/node_modules/**/*`,
        `!${config.source}/strap/public/css`,
        `!${config.source}/strap/public/css/**/*`,
        `!${config.source}/strap/public/js`,
        `!${config.source}/strap/public/js/**/*`
    ] );

    function fixPath( oldPath ){
        var relative = oldPath.replace( `${config.source}${path.sep}strap${path.sep}`, "" );
        var newPath = path.join( config.root, relative );
        var basename = path.basename( oldPath );
        var pathObj = path.parse( newPath );

        if( basename == "proj.js" ){
            pathObj.base = `${config.filename}.js`;
        }
        else if( basename == ".hidden-gitignore" ){
            pathObj.base = ".gitignore";
        }

        return path.format( pathObj );
    }

    function fixData( contents ){
        return contents
            .replace( /%%PROJECTKEY%%/g, config.key )
            .replace( /%%PROJECTNAME%%/g, config.name )
            .replace( /%%FILENAME%%/g, config.filename );
    }

    function copyFile( filename ){
        var newPath = fixPath( filename );
        var newData;

        if( fs.statSync( filename ).isFile() ){
            mkdirp( path.dirname( newPath ), ( e ) => { /* swallow errors */ } );

            fs.readFile( filename, "utf8", ( e, fileContent ) => {
                if( e ){
                    if( !config.silent ){
                        console.log( chalk.red.bold( "BOMBED" ) );
                        console.log( chalk.red( e ) );
                    }
                }
                else{
                    newData = fixData( fileContent );

                    fs.writeFile( newPath, newData, ( e ) => { /* swallow errors */ } );
                }
            } );
        }
    }

    fileList.then(
        ( files ) => {
            if( !config.silent ){
                console.log( chalk.green( `Putting files into ${config.root}` ) );
            }
            files.forEach( copyFile );

            if( asyncDoneBack ){
                asyncDoneBack();
            };
        },
        ( err ) => {
            if( !config.silent ){
                console.log( chalk.red( "BOMBED" ) );
            }
        }
    );
}

// START SETUP PROGRAM

clear();

console.log(
    chalk.rgb( 43, 77, 255 )(
        figlet.textSync( "Bowdrill", { "horizontalLayout": "full" } )
    )
);

inquirer
    .prompt( questions )
    .then( ( responses ) => {
        config = calculateSlugs( responses );
        config = configureRuntime( config );

        preInform( config );
        install( Object.assign( {}, config, { "silent": true } ) );
        install( config, () => {
            postInform( config );
        } );
    } );
