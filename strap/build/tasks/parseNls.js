var glob = require( "glob" );

function convertTranslation( filename, nls, grunt ){
    var parts = filename.split( "/" );
    var storage = nls[ parts[ 0 ] ] || {};
    var restPath = parts.slice( 1 ).join( "/" );

    var l10n = grunt.file.read( `${process.cwd()}/src/content/nls/${filename}` );
    var parsedTranslations = JSON.parse( l10n );

    storage[ restPath ] = parsedTranslations;
    nls[ parts[ 0 ] ] = restPath ? storage : parsedTranslations;

    return nls;
}

module.exports = function templateParserRegistrar( grunt ){
    "use strict";

    grunt.registerTask( "parseNls", "Parse all of the NLS files into a single JSON representation", function nlsParser(){
        var done = this.async();
        var nls = {};

        glob(
            "**/*.json",
            {
                "cwd": process.cwd() + "/src/content/nls",
                "ignore": [
                    "translations\.json"
                ]
            },
            ( err, files ) => {
                if( err ){
                    grunt.fail.fatal( err );
                }
                else{
                    files.forEach(
                        ( filename ) => {
                            nls = convertTranslation( filename, nls, grunt );
                        }
                    );
                }

                grunt.file.write( `${process.cwd()}/src/content/nls/translations.json`, JSON.stringify( nls ) );

                done();
            }
        );
    } );
};
