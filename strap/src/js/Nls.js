import L10n from "./L10n.js";
import translations from "../content/nls/translations.json";

var Nls;

function normalizePath( path, search ){
    if( !search[ path ] ){
        path = `${path}.json`;
    }

    return path;
}

function localize( found ){
    var l10n = new L10n();

    l10n.loadDefinitions( { "*": found } );

    return l10n.localize();
}

Nls = {
    getComponent( path ){
        return localize( translations.components[ normalizePath( path, translations.components ) ] );
    },
    getView( path ){
        return localize( translations.views[ normalizePath( path, translations.views ) ] );
    },

    get( path, prefix = "" ){
        var prefixes = Object.keys( translations );
        var search = translations;
        var parts;

        if( prefixes.includes( prefix ) ){
            search = search[ prefix ];
        }
        else{
            parts = path.split( "/" );

            if( prefixes.includes( parts[ 0 ] ) ){
                search = search[ parts[ 0 ] ];

                path = parts.slice( 1 ).join( "/" );
            }
        }

        return localize( search[ normalizePath( path, search ) ] );
    }
};

export default Nls;
