import { createRouteChange } from "../actions/ROUTE_CHANGE.js";

import Chrome from "../../Chrome.js";
import Store from "../../Store.js";

var Bridge = {
    "steps": {
        injectDefinitionStep( context, next ){
            context.definition = this;

            next();
        },
        setupChromeStep( context, next ){
            var definition = context.definition;

            Chrome.prepare( definition );

            next();
        },
        actionStep( context ){
            Store.get().dispatch( createRouteChange( context ) );
        }
    }
};

export default Bridge;
