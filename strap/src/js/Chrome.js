import Storage from "./Storage";

var Chrome = {
    prepare( definition ){
        Storage.set( "%%PROJECTKEY%%.menus.main.active", definition.menu );
        window.document.title = definition.title ? `${definition.title} ❚ %%PROJECTNAME%%` : "%%PROJECTNAME%%";
    }
};

export default Chrome;
