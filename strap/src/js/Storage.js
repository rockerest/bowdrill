import LocalStorageStrategy from "./common/strategies/LocalStorageStrategy.js";
import RuntimeStorageStrategy from "./common/strategies/RuntimeStorageStrategy.js";

var Storage;

function getStorageStrategy(){
    return LocalStorageStrategy;
}

Storage = {
    "get": getStorageStrategy().get,
    "set": getStorageStrategy().set,
    "remove": getStorageStrategy().remove,
    "Session": RuntimeStorageStrategy
};

export default Storage;
