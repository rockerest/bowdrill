import Ractive from "ractive";

import Templates from "../Templates.js";

var ChromeView = Ractive.extend( {
    "template": Templates.getView( "chrome" )
} );

export default ChromeView;
