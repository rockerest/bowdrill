import Templates from "../Templates.js";
import Nls from "../Nls.js";

var translations = Nls.getView( "home" );
var HomeView = {
    "template": Templates.getView( "home" ),
    data(){
        return {
            translations
        };
    }
};

export default HomeView;
