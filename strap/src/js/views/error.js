import Templates from "../Templates.js";
import Nls from "../Nls.js";

var translations = Nls.getView( "error" );
var ErrorView = {
    "template": Templates.getView( "error" ),
    data(){
        return {
            "error": {},
            translations
        };
    },

    "on": {
        init(){
            this.set( "error", this.mountContext.error || {} );
        }
    }
};

export default ErrorView;
