# Bowdrill
## Simple application bootstrapper

### Getting Started  

```
npx bowdrill
```

Step by step (the installer walks you through everything!):

If you have `npm@5.2.0` or newer:

1. `mkdir myNewProject && cd $_`
2. `npx bowdrill`
3. `npm install && npm run build && npm start`
4. Go to [http://localhost:9876](http://localhost:9876) in a browser.

If you have an older version of `npm`, you'll need to run bowdrill yourself and remove it when it's done.

1. `mkdir myNewProject && cd $_`
2. `npm install bowdrill`
3. `node node_modules/bowdrill`
4. `npm install && npm run build && npm prune && npm start`
5. Go to [http://localhost:9876](http://localhost:9876) in a browser.

----

### [Documentation](https://rockerest.gitlab.io/bowdrill/)
